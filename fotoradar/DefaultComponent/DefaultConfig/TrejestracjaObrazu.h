/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TrejestracjaObrazu
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TrejestracjaObrazu.h
*********************************************************************/

#ifndef TrejestracjaObrazu_H
#define TrejestracjaObrazu_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## package Default

//## class TrejestracjaObrazu
class TrejestracjaObrazu : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedTrejestracjaObrazu;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    TrejestracjaObrazu(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~TrejestracjaObrazu();
    
    ////    Operations    ////
    
    //## operation message_0()
    void message_0();
    
    //## operation rejestrujObraz()
    void rejestrujObraz();
    
    ////    Additional operations    ////
    
    //## auto_generated
    virtual bool startBehavior();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedTrejestracjaObrazu : virtual public AOMInstance {
    DECLARE_META(TrejestracjaObrazu, OMAnimatedTrejestracjaObrazu)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TrejestracjaObrazu.h
*********************************************************************/
