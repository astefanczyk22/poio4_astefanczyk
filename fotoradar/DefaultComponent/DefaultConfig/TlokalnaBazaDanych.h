/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TlokalnaBazaDanych
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TlokalnaBazaDanych.h
*********************************************************************/

#ifndef TlokalnaBazaDanych_H
#define TlokalnaBazaDanych_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## package Default

//## class TlokalnaBazaDanych
class TlokalnaBazaDanych : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedTlokalnaBazaDanych;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    TlokalnaBazaDanych(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~TlokalnaBazaDanych();
    
    ////    Additional operations    ////
    
    //## auto_generated
    virtual bool startBehavior();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedTlokalnaBazaDanych : virtual public AOMInstance {
    DECLARE_META(TlokalnaBazaDanych, OMAnimatedTlokalnaBazaDanych)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TlokalnaBazaDanych.h
*********************************************************************/
