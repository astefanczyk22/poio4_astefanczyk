/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Traport
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/Traport.h
*********************************************************************/

#ifndef Traport_H
#define Traport_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## link itsToperator
class Toperator;

//## package Default

//## class Traport
class Traport {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedTraport;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Traport();
    
    //## auto_generated
    ~Traport();
    
    ////    Operations    ////
    
    //## operation createRaport()
    void createRaport();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Toperator* getItsToperator() const;
    
    //## auto_generated
    void setItsToperator(Toperator* p_Toperator);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Toperator* itsToperator;		//## link itsToperator
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsToperator(Toperator* p_Toperator);
    
    //## auto_generated
    void _setItsToperator(Toperator* p_Toperator);
    
    //## auto_generated
    void _clearItsToperator();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedTraport : virtual public AOMInstance {
    DECLARE_META(Traport, OMAnimatedTraport)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Traport.h
*********************************************************************/
