/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: SystemCentralny
//!	Generated Date	: Wed, 21, Jun 2023  
	File Path	: DefaultComponent\DefaultConfig\SystemCentralny.h
*********************************************************************/

#ifndef SystemCentralny_H
#define SystemCentralny_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "Default.h"
//## package Default

//## actor SystemCentralny
class SystemCentralny {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    SystemCentralny();
    
    //## auto_generated
    ~SystemCentralny();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\SystemCentralny.h
*********************************************************************/
