/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
class TbazaDanych;

//## auto_generated
class TczujnikPredkosci;

//## auto_generated
class Tfotoradar;

//## auto_generated
class TlokalnaBazaDanych;

//## auto_generated
class Toperator;

//## auto_generated
class Traport;

//## auto_generated
class TrejestracjaObrazu;

//## auto_generated
class TsystemCentralny;

//#[ ignore
#define evCallOperator_Default_id 18601

#define evPrzekroczeniePredkosci_Default_id 18602

#define eventmessage_0_Default_id 18603

#define akwizycjaObrazu_Default_id 18604

#define aktualizacjaLokalnychDanych_Default_id 18605

#define evMovingObjectDetection_Default_id 18606

#define evWykrycieObiektu_Default_id 18607

#define evWykrytyObiekt_Default_id 18608
//#]

//## package Default



//## event evCallOperator()
class evCallOperator : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevCallOperator;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evCallOperator();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevCallOperator : virtual public AOMEvent {
    DECLARE_META_EVENT(evCallOperator)
};
//#]
#endif // _OMINSTRUMENT

//## event evPrzekroczeniePredkosci()
class evPrzekroczeniePredkosci : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPrzekroczeniePredkosci;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPrzekroczeniePredkosci();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPrzekroczeniePredkosci : virtual public AOMEvent {
    DECLARE_META_EVENT(evPrzekroczeniePredkosci)
};
//#]
#endif // _OMINSTRUMENT

//## event eventmessage_0()
class eventmessage_0 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedeventmessage_0;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    eventmessage_0();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedeventmessage_0 : virtual public AOMEvent {
    DECLARE_META_EVENT(eventmessage_0)
};
//#]
#endif // _OMINSTRUMENT

//## event akwizycjaObrazu()
class akwizycjaObrazu : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedakwizycjaObrazu;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    akwizycjaObrazu();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedakwizycjaObrazu : virtual public AOMEvent {
    DECLARE_META_EVENT(akwizycjaObrazu)
};
//#]
#endif // _OMINSTRUMENT

//## event aktualizacjaLokalnychDanych()
class aktualizacjaLokalnychDanych : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedaktualizacjaLokalnychDanych;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    aktualizacjaLokalnychDanych();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedaktualizacjaLokalnychDanych : virtual public AOMEvent {
    DECLARE_META_EVENT(aktualizacjaLokalnychDanych)
};
//#]
#endif // _OMINSTRUMENT

//## event evMovingObjectDetection()
class evMovingObjectDetection : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevMovingObjectDetection;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evMovingObjectDetection();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevMovingObjectDetection : virtual public AOMEvent {
    DECLARE_META_EVENT(evMovingObjectDetection)
};
//#]
#endif // _OMINSTRUMENT

//## event evWykrycieObiektu()
class evWykrycieObiektu : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWykrycieObiektu;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWykrycieObiektu();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWykrycieObiektu : virtual public AOMEvent {
    DECLARE_META_EVENT(evWykrycieObiektu)
};
//#]
#endif // _OMINSTRUMENT

//## event evWykrytyObiekt()
class evWykrytyObiekt : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWykrytyObiekt;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWykrytyObiekt();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWykrytyObiekt : virtual public AOMEvent {
    DECLARE_META_EVENT(evWykrytyObiekt)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/
