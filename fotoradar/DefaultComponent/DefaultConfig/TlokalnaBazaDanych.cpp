/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TlokalnaBazaDanych
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TlokalnaBazaDanych.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "TlokalnaBazaDanych.h"
//#[ ignore
#define Default_TlokalnaBazaDanych_TlokalnaBazaDanych_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class TlokalnaBazaDanych
TlokalnaBazaDanych::TlokalnaBazaDanych(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(TlokalnaBazaDanych, TlokalnaBazaDanych(), 0, Default_TlokalnaBazaDanych_TlokalnaBazaDanych_SERIALIZE);
    setActiveContext(theActiveContext, false);
}

TlokalnaBazaDanych::~TlokalnaBazaDanych() {
    NOTIFY_DESTRUCTOR(~TlokalnaBazaDanych, true);
}

bool TlokalnaBazaDanych::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

#ifdef _OMINSTRUMENT
IMPLEMENT_REACTIVE_META_SIMPLE_P(TlokalnaBazaDanych, Default, Default, false, OMAnimatedTlokalnaBazaDanych)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TlokalnaBazaDanych.cpp
*********************************************************************/
