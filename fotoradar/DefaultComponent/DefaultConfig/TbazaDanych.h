/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TbazaDanych
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TbazaDanych.h
*********************************************************************/

#ifndef TbazaDanych_H
#define TbazaDanych_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## package Default

//## class TbazaDanych
class TbazaDanych : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedTbazaDanych;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    TbazaDanych(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~TbazaDanych();
    
    ////    Operations    ////
    
    //## operation message_0()
    void message_0();
    
    //## operation pobierzDane()
    void pobierzDane();
    
    //## operation zaktaulizujDaneLokalne()
    void zaktaulizujDaneLokalne();
    
    ////    Additional operations    ////
    
    //## auto_generated
    virtual bool startBehavior();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedTbazaDanych : virtual public AOMInstance {
    DECLARE_META(TbazaDanych, OMAnimatedTbazaDanych)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TbazaDanych.h
*********************************************************************/
