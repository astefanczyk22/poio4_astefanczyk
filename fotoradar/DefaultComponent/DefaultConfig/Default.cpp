/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "TbazaDanych.h"
//## auto_generated
#include "TczujnikPredkosci.h"
//## auto_generated
#include "Tfotoradar.h"
//## auto_generated
#include "TlokalnaBazaDanych.h"
//## auto_generated
#include "Toperator.h"
//## auto_generated
#include "Traport.h"
//## auto_generated
#include "TrejestracjaObrazu.h"
//## auto_generated
#include "TsystemCentralny.h"
//#[ ignore
#define evCallOperator_SERIALIZE OM_NO_OP

#define evCallOperator_UNSERIALIZE OM_NO_OP

#define evCallOperator_CONSTRUCTOR evCallOperator()

#define evPrzekroczeniePredkosci_SERIALIZE OM_NO_OP

#define evPrzekroczeniePredkosci_UNSERIALIZE OM_NO_OP

#define evPrzekroczeniePredkosci_CONSTRUCTOR evPrzekroczeniePredkosci()

#define eventmessage_0_SERIALIZE OM_NO_OP

#define eventmessage_0_UNSERIALIZE OM_NO_OP

#define eventmessage_0_CONSTRUCTOR eventmessage_0()

#define akwizycjaObrazu_SERIALIZE OM_NO_OP

#define akwizycjaObrazu_UNSERIALIZE OM_NO_OP

#define akwizycjaObrazu_CONSTRUCTOR akwizycjaObrazu()

#define aktualizacjaLokalnychDanych_SERIALIZE OM_NO_OP

#define aktualizacjaLokalnychDanych_UNSERIALIZE OM_NO_OP

#define aktualizacjaLokalnychDanych_CONSTRUCTOR aktualizacjaLokalnychDanych()

#define evMovingObjectDetection_SERIALIZE OM_NO_OP

#define evMovingObjectDetection_UNSERIALIZE OM_NO_OP

#define evMovingObjectDetection_CONSTRUCTOR evMovingObjectDetection()

#define evWykrycieObiektu_SERIALIZE OM_NO_OP

#define evWykrycieObiektu_UNSERIALIZE OM_NO_OP

#define evWykrycieObiektu_CONSTRUCTOR evWykrycieObiektu()

#define evWykrytyObiekt_SERIALIZE OM_NO_OP

#define evWykrytyObiekt_UNSERIALIZE OM_NO_OP

#define evWykrytyObiekt_CONSTRUCTOR evWykrytyObiekt()
//#]

//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event evCallOperator()
evCallOperator::evCallOperator() {
    NOTIFY_EVENT_CONSTRUCTOR(evCallOperator)
    setId(evCallOperator_Default_id);
}

bool evCallOperator::isTypeOf(const short id) const {
    return (evCallOperator_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evCallOperator, Default, Default, evCallOperator())

//## event evPrzekroczeniePredkosci()
evPrzekroczeniePredkosci::evPrzekroczeniePredkosci() {
    NOTIFY_EVENT_CONSTRUCTOR(evPrzekroczeniePredkosci)
    setId(evPrzekroczeniePredkosci_Default_id);
}

bool evPrzekroczeniePredkosci::isTypeOf(const short id) const {
    return (evPrzekroczeniePredkosci_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evPrzekroczeniePredkosci, Default, Default, evPrzekroczeniePredkosci())

//## event eventmessage_0()
eventmessage_0::eventmessage_0() {
    NOTIFY_EVENT_CONSTRUCTOR(eventmessage_0)
    setId(eventmessage_0_Default_id);
}

bool eventmessage_0::isTypeOf(const short id) const {
    return (eventmessage_0_Default_id==id);
}

IMPLEMENT_META_EVENT_P(eventmessage_0, Default, Default, eventmessage_0())

//## event akwizycjaObrazu()
akwizycjaObrazu::akwizycjaObrazu() {
    NOTIFY_EVENT_CONSTRUCTOR(akwizycjaObrazu)
    setId(akwizycjaObrazu_Default_id);
}

bool akwizycjaObrazu::isTypeOf(const short id) const {
    return (akwizycjaObrazu_Default_id==id);
}

IMPLEMENT_META_EVENT_P(akwizycjaObrazu, Default, Default, akwizycjaObrazu())

//## event aktualizacjaLokalnychDanych()
aktualizacjaLokalnychDanych::aktualizacjaLokalnychDanych() {
    NOTIFY_EVENT_CONSTRUCTOR(aktualizacjaLokalnychDanych)
    setId(aktualizacjaLokalnychDanych_Default_id);
}

bool aktualizacjaLokalnychDanych::isTypeOf(const short id) const {
    return (aktualizacjaLokalnychDanych_Default_id==id);
}

IMPLEMENT_META_EVENT_P(aktualizacjaLokalnychDanych, Default, Default, aktualizacjaLokalnychDanych())

//## event evMovingObjectDetection()
evMovingObjectDetection::evMovingObjectDetection() {
    NOTIFY_EVENT_CONSTRUCTOR(evMovingObjectDetection)
    setId(evMovingObjectDetection_Default_id);
}

bool evMovingObjectDetection::isTypeOf(const short id) const {
    return (evMovingObjectDetection_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evMovingObjectDetection, Default, Default, evMovingObjectDetection())

//## event evWykrycieObiektu()
evWykrycieObiektu::evWykrycieObiektu() {
    NOTIFY_EVENT_CONSTRUCTOR(evWykrycieObiektu)
    setId(evWykrycieObiektu_Default_id);
}

bool evWykrycieObiektu::isTypeOf(const short id) const {
    return (evWykrycieObiektu_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWykrycieObiektu, Default, Default, evWykrycieObiektu())

//## event evWykrytyObiekt()
evWykrytyObiekt::evWykrytyObiekt() {
    NOTIFY_EVENT_CONSTRUCTOR(evWykrytyObiekt)
    setId(evWykrytyObiekt_Default_id);
}

bool evWykrytyObiekt::isTypeOf(const short id) const {
    return (evWykrytyObiekt_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWykrytyObiekt, Default, Default, evWykrytyObiekt())

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/
