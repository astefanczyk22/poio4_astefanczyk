/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TbazaDanych
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TbazaDanych.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "TbazaDanych.h"
//#[ ignore
#define Default_TbazaDanych_TbazaDanych_SERIALIZE OM_NO_OP

#define Default_TbazaDanych_message_0_SERIALIZE OM_NO_OP

#define Default_TbazaDanych_pobierzDane_SERIALIZE OM_NO_OP

#define Default_TbazaDanych_zaktaulizujDaneLokalne_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class TbazaDanych
TbazaDanych::TbazaDanych(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(TbazaDanych, TbazaDanych(), 0, Default_TbazaDanych_TbazaDanych_SERIALIZE);
    setActiveContext(theActiveContext, false);
}

TbazaDanych::~TbazaDanych() {
    NOTIFY_DESTRUCTOR(~TbazaDanych, true);
}

void TbazaDanych::message_0() {
    NOTIFY_OPERATION(message_0, message_0(), 0, Default_TbazaDanych_message_0_SERIALIZE);
    //#[ operation message_0()
    //#]
}

void TbazaDanych::pobierzDane() {
    NOTIFY_OPERATION(pobierzDane, pobierzDane(), 0, Default_TbazaDanych_pobierzDane_SERIALIZE);
    //#[ operation pobierzDane()
    //#]
}

void TbazaDanych::zaktaulizujDaneLokalne() {
    NOTIFY_OPERATION(zaktaulizujDaneLokalne, zaktaulizujDaneLokalne(), 0, Default_TbazaDanych_zaktaulizujDaneLokalne_SERIALIZE);
    //#[ operation zaktaulizujDaneLokalne()
    //#]
}

bool TbazaDanych::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

#ifdef _OMINSTRUMENT
IMPLEMENT_REACTIVE_META_SIMPLE_P(TbazaDanych, Default, Default, false, OMAnimatedTbazaDanych)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TbazaDanych.cpp
*********************************************************************/
