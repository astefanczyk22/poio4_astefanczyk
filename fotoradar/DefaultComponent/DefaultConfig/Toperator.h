/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Toperator
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/Toperator.h
*********************************************************************/

#ifndef Toperator_H
#define Toperator_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsTraport
class Traport;

//## link itsTsystemCentralny
class TsystemCentralny;

//## package Default

//## class Toperator
class Toperator : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedToperator;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Toperator(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Toperator();
    
    ////    Operations    ////
    
    //## operation bladAutoryzacji()
    void bladAutoryzacji();
    
    //## operation message_0()
    void message_0();
    
    //## operation sukcesAutoryzacji()
    void sukcesAutoryzacji();
    
    //## operation utworzZapytanie()
    void utworzZapytanie();
    
    //## operation uwierzytelnienieUzytkownika()
    void uwierzytelnienieUzytkownika();
    
    //## operation wyslijOdpowiedz()
    void wyslijOdpowiedz();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Traport* getItsTraport() const;
    
    //## auto_generated
    void setItsTraport(Traport* p_Traport);
    
    //## auto_generated
    Traport* getItsTraport_1() const;
    
    //## auto_generated
    void setItsTraport_1(Traport* p_Traport);
    
    //## auto_generated
    Traport* getItsTraport_2() const;
    
    //## auto_generated
    void setItsTraport_2(Traport* p_Traport);
    
    //## auto_generated
    TsystemCentralny* getItsTsystemCentralny() const;
    
    //## auto_generated
    void setItsTsystemCentralny(TsystemCentralny* p_TsystemCentralny);
    
    //## auto_generated
    TsystemCentralny* getItsTsystemCentralny_1() const;
    
    //## auto_generated
    void setItsTsystemCentralny_1(TsystemCentralny* p_TsystemCentralny);
    
    //## auto_generated
    TsystemCentralny* getItsTsystemCentralny_2() const;
    
    //## auto_generated
    void setItsTsystemCentralny_2(TsystemCentralny* p_TsystemCentralny);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Traport* itsTraport;		//## link itsTraport
    
    Traport* itsTraport_1;		//## link itsTraport_1
    
    Traport* itsTraport_2;		//## link itsTraport_2
    
    TsystemCentralny* itsTsystemCentralny;		//## link itsTsystemCentralny
    
    TsystemCentralny* itsTsystemCentralny_1;		//## link itsTsystemCentralny_1
    
    TsystemCentralny* itsTsystemCentralny_2;		//## link itsTsystemCentralny_2
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsTraport(Traport* p_Traport);
    
    //## auto_generated
    void _setItsTraport(Traport* p_Traport);
    
    //## auto_generated
    void _clearItsTraport();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedToperator : virtual public AOMInstance {
    DECLARE_META(Toperator, OMAnimatedToperator)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Toperator.h
*********************************************************************/
