/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TrejestracjaObrazu
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TrejestracjaObrazu.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "TrejestracjaObrazu.h"
//#[ ignore
#define Default_TrejestracjaObrazu_TrejestracjaObrazu_SERIALIZE OM_NO_OP

#define Default_TrejestracjaObrazu_message_0_SERIALIZE OM_NO_OP

#define Default_TrejestracjaObrazu_rejestrujObraz_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class TrejestracjaObrazu
TrejestracjaObrazu::TrejestracjaObrazu(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(TrejestracjaObrazu, TrejestracjaObrazu(), 0, Default_TrejestracjaObrazu_TrejestracjaObrazu_SERIALIZE);
    setActiveContext(theActiveContext, false);
}

TrejestracjaObrazu::~TrejestracjaObrazu() {
    NOTIFY_DESTRUCTOR(~TrejestracjaObrazu, true);
}

void TrejestracjaObrazu::message_0() {
    NOTIFY_OPERATION(message_0, message_0(), 0, Default_TrejestracjaObrazu_message_0_SERIALIZE);
    //#[ operation message_0()
    //#]
}

void TrejestracjaObrazu::rejestrujObraz() {
    NOTIFY_OPERATION(rejestrujObraz, rejestrujObraz(), 0, Default_TrejestracjaObrazu_rejestrujObraz_SERIALIZE);
    //#[ operation rejestrujObraz()
    //#]
}

bool TrejestracjaObrazu::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

#ifdef _OMINSTRUMENT
IMPLEMENT_REACTIVE_META_SIMPLE_P(TrejestracjaObrazu, Default, Default, false, OMAnimatedTrejestracjaObrazu)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TrejestracjaObrazu.cpp
*********************************************************************/
