/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TczujnikPredkosci
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TczujnikPredkosci.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "TczujnikPredkosci.h"
//## link itsTfotoradar
#include "Tfotoradar.h"
//## link itsTrejestracjaObrazu
#include "TrejestracjaObrazu.h"
//#[ ignore
#define Default_TczujnikPredkosci_TczujnikPredkosci_SERIALIZE OM_NO_OP

#define Default_TczujnikPredkosci_evWykrycieObiektu_SERIALIZE OM_NO_OP

#define Default_TczujnikPredkosci_pomiarPredkosci_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class TczujnikPredkosci
TczujnikPredkosci::TczujnikPredkosci(IOxfActive* theActiveContext) : OgraniczeniePredkosci(70) {
    NOTIFY_REACTIVE_CONSTRUCTOR(TczujnikPredkosci, TczujnikPredkosci(), 0, Default_TczujnikPredkosci_TczujnikPredkosci_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsTfotoradar = NULL;
    itsTrejestracjaObrazu = NULL;
    initStatechart();
}

TczujnikPredkosci::~TczujnikPredkosci() {
    NOTIFY_DESTRUCTOR(~TczujnikPredkosci, true);
    cleanUpRelations();
}

Tfotoradar* TczujnikPredkosci::getItsTfotoradar() const {
    return itsTfotoradar;
}

void TczujnikPredkosci::setItsTfotoradar(Tfotoradar* p_Tfotoradar) {
    if(p_Tfotoradar != NULL)
        {
            p_Tfotoradar->_setItsTczujnikPredkosci(this);
        }
    _setItsTfotoradar(p_Tfotoradar);
}

TrejestracjaObrazu* TczujnikPredkosci::getItsTrejestracjaObrazu() const {
    return itsTrejestracjaObrazu;
}

void TczujnikPredkosci::setItsTrejestracjaObrazu(TrejestracjaObrazu* p_TrejestracjaObrazu) {
    itsTrejestracjaObrazu = p_TrejestracjaObrazu;
    if(p_TrejestracjaObrazu != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTrejestracjaObrazu", p_TrejestracjaObrazu, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTrejestracjaObrazu");
        }
}

bool TczujnikPredkosci::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void TczujnikPredkosci::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void TczujnikPredkosci::cleanUpRelations() {
    if(itsTfotoradar != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTfotoradar");
            TczujnikPredkosci* p_TczujnikPredkosci = itsTfotoradar->getItsTczujnikPredkosci();
            if(p_TczujnikPredkosci != NULL)
                {
                    itsTfotoradar->__setItsTczujnikPredkosci(NULL);
                }
            itsTfotoradar = NULL;
        }
    if(itsTrejestracjaObrazu != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTrejestracjaObrazu");
            itsTrejestracjaObrazu = NULL;
        }
}

void TczujnikPredkosci::__setItsTfotoradar(Tfotoradar* p_Tfotoradar) {
    itsTfotoradar = p_Tfotoradar;
    if(p_Tfotoradar != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTfotoradar", p_Tfotoradar, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTfotoradar");
        }
}

void TczujnikPredkosci::_setItsTfotoradar(Tfotoradar* p_Tfotoradar) {
    if(itsTfotoradar != NULL)
        {
            itsTfotoradar->__setItsTczujnikPredkosci(NULL);
        }
    __setItsTfotoradar(p_Tfotoradar);
}

void TczujnikPredkosci::_clearItsTfotoradar() {
    NOTIFY_RELATION_CLEARED("itsTfotoradar");
    itsTfotoradar = NULL;
}

double TczujnikPredkosci::getOgraniczeniePredkosci() const {
    return OgraniczeniePredkosci;
}

double TczujnikPredkosci::getZmierzonaPredkosc() const {
    return ZmierzonaPredkosc;
}

void TczujnikPredkosci::setZmierzonaPredkosc(double p_ZmierzonaPredkosc) {
    ZmierzonaPredkosc = p_ZmierzonaPredkosc;
}

double TczujnikPredkosci::pomiarPredkosci() {
    NOTIFY_OPERATION(pomiarPredkosci, pomiarPredkosci(), 0, Default_TczujnikPredkosci_pomiarPredkosci_SERIALIZE);
    //#[ operation pomiarPredkosci()
    double zmierzona_predkosc = 50 + (static_cast<double>(rand()) / RAND_MAX) * 40;
    
    return zmierzona_predkosc;
    //#]
}

void TczujnikPredkosci::setOgraniczeniePredkosci(double p_OgraniczeniePredkosci) {
    OgraniczeniePredkosci = p_OgraniczeniePredkosci;
}

//#[ ignore
#undef OM_RET_TYPE
#define OM_RET_TYPE 
#undef OM_SER_RET
#define OM_SER_RET(val) 
#undef OM_SER_OUT
#define OM_SER_OUT 
//#]


void TczujnikPredkosci::evWykrycieObiektu() {
    NOTIFY_TRIGGERED_OPERATION(evWykrycieObiektu, evWykrycieObiektu(), 0, Default_TczujnikPredkosci_evWykrycieObiektu_SERIALIZE);
    evWykrycieObiektu_TczujnikPredkosci_Event triggerEvent;
    handleTrigger(&triggerEvent);
    OM_RETURN_VOID;
}

void TczujnikPredkosci::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("2");
        NOTIFY_STATE_ENTERED("ROOT.oczekiwanie");
        rootState_subState = oczekiwanie;
        rootState_active = oczekiwanie;
        //#[ state oczekiwanie.(Entry) 
        std::cout << std::endl << "oczekiwanie na pojazd" << std::endl << std::endl;
        //#]
        NOTIFY_TRANSITION_TERMINATED("2");
    }
}

IOxfReactive::TakeEventStatus TczujnikPredkosci::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State oczekiwanie
        case oczekiwanie:
        {
            if(IS_EVENT_TYPE_OF(evWykrytyObiekt_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    NOTIFY_STATE_EXITED("ROOT.oczekiwanie");
                    NOTIFY_STATE_ENTERED("ROOT.stanFotoradaru");
                    pushNullTransition();
                    rootState_subState = stanFotoradaru;
                    rootState_active = stanFotoradaru;
                    //#[ state stanFotoradaru.(Entry) 
                    this->ZmierzonaPredkosc = this->pomiarPredkosci();   
                    std::cout << "Wykryto pojazd" << std::endl;
                    std::cout << "Predkosc wykrytego pojazdu: " << this->ZmierzonaPredkosc << std::endl; 
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
        }
        break;
        // State stanFotoradaru
        case stanFotoradaru:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    //## transition 0 
                    if(this->ZmierzonaPredkosc > this->OgraniczeniePredkosci)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            NOTIFY_TRANSITION_STARTED("0");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.stanFotoradaru");
                            NOTIFY_STATE_ENTERED("ROOT.przekroczeniePredkosci");
                            pushNullTransition();
                            rootState_subState = przekroczeniePredkosci;
                            rootState_active = przekroczeniePredkosci;
                            //#[ state przekroczeniePredkosci.(Entry) 
                            std::cout << "Pojazd przekroczyl ograniczenie predkosci: " << this->OgraniczeniePredkosci << std::endl; 
                            std::cout << "Zapisywanie przechwyconego obrazu do lokalnej bazy danych" << std::endl;
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("0");
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                    else
                        {
                            //## transition 1 
                            if(this->ZmierzonaPredkosc <= this->OgraniczeniePredkosci)
                                {
                                    NOTIFY_TRANSITION_STARTED("3");
                                    NOTIFY_TRANSITION_STARTED("1");
                                    popNullTransition();
                                    NOTIFY_STATE_EXITED("ROOT.stanFotoradaru");
                                    NOTIFY_STATE_ENTERED("ROOT.odpowiedniaPredkosc");
                                    pushNullTransition();
                                    rootState_subState = odpowiedniaPredkosc;
                                    rootState_active = odpowiedniaPredkosc;
                                    //#[ state odpowiedniaPredkosc.(Entry) 
                                    std::cout << "Pojazd porusza sie zgodnie z przepisami" << std::endl;
                                    //#]
                                    NOTIFY_TRANSITION_TERMINATED("1");
                                    NOTIFY_TRANSITION_TERMINATED("3");
                                    res = eventConsumed;
                                }
                        }
                }
            
        }
        break;
        // State przekroczeniePredkosci
        case przekroczeniePredkosci:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.przekroczeniePredkosci");
                    NOTIFY_STATE_ENTERED("ROOT.oczekiwanie");
                    rootState_subState = oczekiwanie;
                    rootState_active = oczekiwanie;
                    //#[ state oczekiwanie.(Entry) 
                    std::cout << std::endl << "oczekiwanie na pojazd" << std::endl << std::endl;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            
        }
        break;
        // State odpowiedniaPredkosc
        case odpowiedniaPredkosc:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.odpowiedniaPredkosc");
                    NOTIFY_STATE_ENTERED("ROOT.oczekiwanie");
                    rootState_subState = oczekiwanie;
                    rootState_active = oczekiwanie;
                    //#[ state oczekiwanie.(Entry) 
                    std::cout << std::endl << "oczekiwanie na pojazd" << std::endl << std::endl;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedTczujnikPredkosci::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("ZmierzonaPredkosc", x2String(myReal->ZmierzonaPredkosc));
    aomsAttributes->addAttribute("OgraniczeniePredkosci", x2String(myReal->OgraniczeniePredkosci));
}

void OMAnimatedTczujnikPredkosci::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsTfotoradar", false, true);
    if(myReal->itsTfotoradar)
        {
            aomsRelations->ADD_ITEM(myReal->itsTfotoradar);
        }
    aomsRelations->addRelation("itsTrejestracjaObrazu", false, true);
    if(myReal->itsTrejestracjaObrazu)
        {
            aomsRelations->ADD_ITEM(myReal->itsTrejestracjaObrazu);
        }
}

void OMAnimatedTczujnikPredkosci::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case TczujnikPredkosci::oczekiwanie:
        {
            oczekiwanie_serializeStates(aomsState);
        }
        break;
        case TczujnikPredkosci::stanFotoradaru:
        {
            stanFotoradaru_serializeStates(aomsState);
        }
        break;
        case TczujnikPredkosci::przekroczeniePredkosci:
        {
            przekroczeniePredkosci_serializeStates(aomsState);
        }
        break;
        case TczujnikPredkosci::odpowiedniaPredkosc:
        {
            odpowiedniaPredkosc_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedTczujnikPredkosci::stanFotoradaru_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.stanFotoradaru");
}

void OMAnimatedTczujnikPredkosci::przekroczeniePredkosci_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.przekroczeniePredkosci");
}

void OMAnimatedTczujnikPredkosci::odpowiedniaPredkosc_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.odpowiedniaPredkosc");
}

void OMAnimatedTczujnikPredkosci::oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.oczekiwanie");
}
//#]

IMPLEMENT_REACTIVE_META_P(TczujnikPredkosci, Default, Default, false, OMAnimatedTczujnikPredkosci)
#endif // _OMINSTRUMENT

//#[ ignore
evWykrycieObiektu_TczujnikPredkosci_Event::evWykrycieObiektu_TczujnikPredkosci_Event() {
    setId(evWykrycieObiektu_TczujnikPredkosci_Event_id);
}
//#]

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TczujnikPredkosci.cpp
*********************************************************************/
