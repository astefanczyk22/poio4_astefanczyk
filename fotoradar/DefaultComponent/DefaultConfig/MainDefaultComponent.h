/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DefaultConfig
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/MainDefaultComponent.h
*********************************************************************/

#ifndef MainDefaultComponent_H
#define MainDefaultComponent_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/MainDefaultComponent.h
*********************************************************************/
