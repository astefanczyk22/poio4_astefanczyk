/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: actor_19
//!	Generated Date	: Wed, 21, Jun 2023  
	File Path	: DefaultComponent\DefaultConfig\actor_19.h
*********************************************************************/

#ifndef actor_19_H
#define actor_19_H

//## auto_generated
#include <oxf\oxf.h>
//## package Default

//## actor actor_19
class actor_19 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    actor_19();
    
    //## auto_generated
    ~actor_19();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\actor_19.h
*********************************************************************/
