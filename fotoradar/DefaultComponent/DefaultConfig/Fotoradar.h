/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Fotoradar
//!	Generated Date	: Wed, 21, Jun 2023  
	File Path	: DefaultComponent\DefaultConfig\Fotoradar.h
*********************************************************************/

#ifndef Fotoradar_H
#define Fotoradar_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "Default.h"
//## package Default

//## actor Fotoradar
class Fotoradar {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Fotoradar();
    
    //## auto_generated
    ~Fotoradar();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\Fotoradar.h
*********************************************************************/
