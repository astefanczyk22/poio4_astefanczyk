/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Toperator
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/Toperator.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Toperator.h"
//## link itsTraport
#include "Traport.h"
//## link itsTsystemCentralny
#include "TsystemCentralny.h"
//#[ ignore
#define Default_Toperator_Toperator_SERIALIZE OM_NO_OP

#define Default_Toperator_bladAutoryzacji_SERIALIZE OM_NO_OP

#define Default_Toperator_message_0_SERIALIZE OM_NO_OP

#define Default_Toperator_sukcesAutoryzacji_SERIALIZE OM_NO_OP

#define Default_Toperator_utworzZapytanie_SERIALIZE OM_NO_OP

#define Default_Toperator_uwierzytelnienieUzytkownika_SERIALIZE OM_NO_OP

#define Default_Toperator_wyslijOdpowiedz_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Toperator
Toperator::Toperator(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Toperator, Toperator(), 0, Default_Toperator_Toperator_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsTraport = NULL;
    itsTraport_1 = NULL;
    itsTraport_2 = NULL;
    itsTsystemCentralny = NULL;
    itsTsystemCentralny_1 = NULL;
    itsTsystemCentralny_2 = NULL;
}

Toperator::~Toperator() {
    NOTIFY_DESTRUCTOR(~Toperator, true);
    cleanUpRelations();
}

void Toperator::bladAutoryzacji() {
    NOTIFY_OPERATION(bladAutoryzacji, bladAutoryzacji(), 0, Default_Toperator_bladAutoryzacji_SERIALIZE);
    //#[ operation bladAutoryzacji()
    //#]
}

void Toperator::message_0() {
    NOTIFY_OPERATION(message_0, message_0(), 0, Default_Toperator_message_0_SERIALIZE);
    //#[ operation message_0()
    //#]
}

void Toperator::sukcesAutoryzacji() {
    NOTIFY_OPERATION(sukcesAutoryzacji, sukcesAutoryzacji(), 0, Default_Toperator_sukcesAutoryzacji_SERIALIZE);
    //#[ operation sukcesAutoryzacji()
    //#]
}

void Toperator::utworzZapytanie() {
    NOTIFY_OPERATION(utworzZapytanie, utworzZapytanie(), 0, Default_Toperator_utworzZapytanie_SERIALIZE);
    //#[ operation utworzZapytanie()
    //#]
}

void Toperator::uwierzytelnienieUzytkownika() {
    NOTIFY_OPERATION(uwierzytelnienieUzytkownika, uwierzytelnienieUzytkownika(), 0, Default_Toperator_uwierzytelnienieUzytkownika_SERIALIZE);
    //#[ operation uwierzytelnienieUzytkownika()
    //#]
}

void Toperator::wyslijOdpowiedz() {
    NOTIFY_OPERATION(wyslijOdpowiedz, wyslijOdpowiedz(), 0, Default_Toperator_wyslijOdpowiedz_SERIALIZE);
    //#[ operation wyslijOdpowiedz()
    //#]
}

Traport* Toperator::getItsTraport() const {
    return itsTraport;
}

void Toperator::setItsTraport(Traport* p_Traport) {
    if(p_Traport != NULL)
        {
            p_Traport->_setItsToperator(this);
        }
    _setItsTraport(p_Traport);
}

Traport* Toperator::getItsTraport_1() const {
    return itsTraport_1;
}

void Toperator::setItsTraport_1(Traport* p_Traport) {
    itsTraport_1 = p_Traport;
    if(p_Traport != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTraport_1", p_Traport, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTraport_1");
        }
}

Traport* Toperator::getItsTraport_2() const {
    return itsTraport_2;
}

void Toperator::setItsTraport_2(Traport* p_Traport) {
    itsTraport_2 = p_Traport;
    if(p_Traport != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTraport_2", p_Traport, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTraport_2");
        }
}

TsystemCentralny* Toperator::getItsTsystemCentralny() const {
    return itsTsystemCentralny;
}

void Toperator::setItsTsystemCentralny(TsystemCentralny* p_TsystemCentralny) {
    itsTsystemCentralny = p_TsystemCentralny;
    if(p_TsystemCentralny != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTsystemCentralny", p_TsystemCentralny, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTsystemCentralny");
        }
}

TsystemCentralny* Toperator::getItsTsystemCentralny_1() const {
    return itsTsystemCentralny_1;
}

void Toperator::setItsTsystemCentralny_1(TsystemCentralny* p_TsystemCentralny) {
    itsTsystemCentralny_1 = p_TsystemCentralny;
    if(p_TsystemCentralny != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTsystemCentralny_1", p_TsystemCentralny, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTsystemCentralny_1");
        }
}

TsystemCentralny* Toperator::getItsTsystemCentralny_2() const {
    return itsTsystemCentralny_2;
}

void Toperator::setItsTsystemCentralny_2(TsystemCentralny* p_TsystemCentralny) {
    itsTsystemCentralny_2 = p_TsystemCentralny;
    if(p_TsystemCentralny != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTsystemCentralny_2", p_TsystemCentralny, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTsystemCentralny_2");
        }
}

bool Toperator::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Toperator::cleanUpRelations() {
    if(itsTraport != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTraport");
            Toperator* p_Toperator = itsTraport->getItsToperator();
            if(p_Toperator != NULL)
                {
                    itsTraport->__setItsToperator(NULL);
                }
            itsTraport = NULL;
        }
    if(itsTraport_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTraport_1");
            itsTraport_1 = NULL;
        }
    if(itsTraport_2 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTraport_2");
            itsTraport_2 = NULL;
        }
    if(itsTsystemCentralny != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTsystemCentralny");
            itsTsystemCentralny = NULL;
        }
    if(itsTsystemCentralny_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTsystemCentralny_1");
            itsTsystemCentralny_1 = NULL;
        }
    if(itsTsystemCentralny_2 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTsystemCentralny_2");
            itsTsystemCentralny_2 = NULL;
        }
}

void Toperator::__setItsTraport(Traport* p_Traport) {
    itsTraport = p_Traport;
    if(p_Traport != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTraport", p_Traport, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTraport");
        }
}

void Toperator::_setItsTraport(Traport* p_Traport) {
    if(itsTraport != NULL)
        {
            itsTraport->__setItsToperator(NULL);
        }
    __setItsTraport(p_Traport);
}

void Toperator::_clearItsTraport() {
    NOTIFY_RELATION_CLEARED("itsTraport");
    itsTraport = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedToperator::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsTraport", false, true);
    if(myReal->itsTraport)
        {
            aomsRelations->ADD_ITEM(myReal->itsTraport);
        }
    aomsRelations->addRelation("itsTraport_1", false, true);
    if(myReal->itsTraport_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsTraport_1);
        }
    aomsRelations->addRelation("itsTsystemCentralny", false, true);
    if(myReal->itsTsystemCentralny)
        {
            aomsRelations->ADD_ITEM(myReal->itsTsystemCentralny);
        }
    aomsRelations->addRelation("itsTsystemCentralny_1", false, true);
    if(myReal->itsTsystemCentralny_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsTsystemCentralny_1);
        }
    aomsRelations->addRelation("itsTsystemCentralny_2", false, true);
    if(myReal->itsTsystemCentralny_2)
        {
            aomsRelations->ADD_ITEM(myReal->itsTsystemCentralny_2);
        }
    aomsRelations->addRelation("itsTraport_2", false, true);
    if(myReal->itsTraport_2)
        {
            aomsRelations->ADD_ITEM(myReal->itsTraport_2);
        }
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(Toperator, Default, Default, false, OMAnimatedToperator)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Toperator.cpp
*********************************************************************/
