/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: OperatorRadaru
//!	Generated Date	: Wed, 21, Jun 2023  
	File Path	: DefaultComponent\DefaultConfig\OperatorRadaru.h
*********************************************************************/

#ifndef OperatorRadaru_H
#define OperatorRadaru_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "Default.h"
//## package Default

//## actor OperatorRadaru
class OperatorRadaru {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    OperatorRadaru();
    
    //## auto_generated
    ~OperatorRadaru();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent\DefaultConfig\OperatorRadaru.h
*********************************************************************/
