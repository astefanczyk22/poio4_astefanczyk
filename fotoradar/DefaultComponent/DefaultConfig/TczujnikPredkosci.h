/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TczujnikPredkosci
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TczujnikPredkosci.h
*********************************************************************/

#ifndef TczujnikPredkosci_H
#define TczujnikPredkosci_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/omthread.h>
//#[ ignore
#define evWykrycieObiektu_TczujnikPredkosci_Event_id 31000
//#]

//## link itsTfotoradar
class Tfotoradar;

//## link itsTrejestracjaObrazu
class TrejestracjaObrazu;

//## package Default

//## class TczujnikPredkosci
class TczujnikPredkosci : public OMReactive {
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedTczujnikPredkosci;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    TczujnikPredkosci(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~TczujnikPredkosci();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Tfotoradar* getItsTfotoradar() const;
    
    //## auto_generated
    void setItsTfotoradar(Tfotoradar* p_Tfotoradar);
    
    //## auto_generated
    TrejestracjaObrazu* getItsTrejestracjaObrazu() const;
    
    //## auto_generated
    void setItsTrejestracjaObrazu(TrejestracjaObrazu* p_TrejestracjaObrazu);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Tfotoradar* itsTfotoradar;		//## link itsTfotoradar
    
    TrejestracjaObrazu* itsTrejestracjaObrazu;		//## link itsTrejestracjaObrazu
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsTfotoradar(Tfotoradar* p_Tfotoradar);
    
    //## auto_generated
    void _setItsTfotoradar(Tfotoradar* p_Tfotoradar);
    
    //## auto_generated
    void _clearItsTfotoradar();
    
    ////    Framework    ////
    
    //## auto_generated
    double getOgraniczeniePredkosci() const;
    
    //## auto_generated
    double getZmierzonaPredkosc() const;
    
    //## auto_generated
    void setZmierzonaPredkosc(double p_ZmierzonaPredkosc);

protected :

    double OgraniczeniePredkosci;		//## attribute OgraniczeniePredkosci
    
    double ZmierzonaPredkosc;		//## attribute ZmierzonaPredkosc

public :

    //## operation pomiarPredkosci()
    double pomiarPredkosci();
    
    //## auto_generated
    void setOgraniczeniePredkosci(double p_OgraniczeniePredkosci);
    
    //## TriggeredOperation evWykrycieObiektu()
    void evWykrycieObiektu();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // stanFotoradaru:
    //## statechart_method
    inline bool stanFotoradaru_IN() const;
    
    // przekroczeniePredkosci:
    //## statechart_method
    inline bool przekroczeniePredkosci_IN() const;
    
    // odpowiedniaPredkosc:
    //## statechart_method
    inline bool odpowiedniaPredkosc_IN() const;
    
    // oczekiwanie:
    //## statechart_method
    inline bool oczekiwanie_IN() const;

protected :

//#[ ignore
    enum TczujnikPredkosci_Enum {
        OMNonState = 0,
        stanFotoradaru = 1,
        przekroczeniePredkosci = 2,
        odpowiedniaPredkosc = 3,
        oczekiwanie = 4
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedTczujnikPredkosci : virtual public AOMInstance {
    DECLARE_REACTIVE_META(TczujnikPredkosci, OMAnimatedTczujnikPredkosci)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void stanFotoradaru_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void przekroczeniePredkosci_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void odpowiedniaPredkosc_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void oczekiwanie_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

//#[ ignore
class evWykrycieObiektu_TczujnikPredkosci_Event : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    evWykrycieObiektu_TczujnikPredkosci_Event();
};
//#]

inline bool TczujnikPredkosci::rootState_IN() const {
    return true;
}

inline bool TczujnikPredkosci::stanFotoradaru_IN() const {
    return rootState_subState == stanFotoradaru;
}

inline bool TczujnikPredkosci::przekroczeniePredkosci_IN() const {
    return rootState_subState == przekroczeniePredkosci;
}

inline bool TczujnikPredkosci::odpowiedniaPredkosc_IN() const {
    return rootState_subState == odpowiedniaPredkosc;
}

inline bool TczujnikPredkosci::oczekiwanie_IN() const {
    return rootState_subState == oczekiwanie;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TczujnikPredkosci.h
*********************************************************************/
