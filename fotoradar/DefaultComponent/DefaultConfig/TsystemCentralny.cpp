/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TsystemCentralny
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TsystemCentralny.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "TsystemCentralny.h"
//## link itsTbazaDanych
#include "TbazaDanych.h"
//## link itsTfotoradar
#include "Tfotoradar.h"
//#[ ignore
#define Default_TsystemCentralny_TsystemCentralny_SERIALIZE OM_NO_OP

#define Default_TsystemCentralny_autoryzacjaUytkownika_SERIALIZE OM_NO_OP

#define Default_TsystemCentralny_autoryzacjaUzytkownika_SERIALIZE OM_NO_OP

#define Default_TsystemCentralny_message_0_SERIALIZE OM_NO_OP

#define Default_TsystemCentralny_przetwarzajZapytanie_SERIALIZE OM_NO_OP

#define Default_TsystemCentralny_przkazDane_SERIALIZE OM_NO_OP

#define Default_TsystemCentralny_wprowadzenieDanych_SERIALIZE OM_NO_OP

#define Default_TsystemCentralny_wyslijOdpowiedz_SERIALIZE OM_NO_OP

#define Default_TsystemCentralny_wyslijZapytanie_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class TsystemCentralny
TsystemCentralny::TsystemCentralny() {
    NOTIFY_CONSTRUCTOR(TsystemCentralny, TsystemCentralny(), 0, Default_TsystemCentralny_TsystemCentralny_SERIALIZE);
    itsTbazaDanych = NULL;
    itsTbazaDanych_1 = NULL;
    itsTfotoradar = NULL;
}

TsystemCentralny::~TsystemCentralny() {
    NOTIFY_DESTRUCTOR(~TsystemCentralny, true);
    cleanUpRelations();
}

void TsystemCentralny::autoryzacjaUytkownika() {
    NOTIFY_OPERATION(autoryzacjaUytkownika, autoryzacjaUytkownika(), 0, Default_TsystemCentralny_autoryzacjaUytkownika_SERIALIZE);
    //#[ operation autoryzacjaUytkownika()
    //#]
}

void TsystemCentralny::autoryzacjaUzytkownika() {
    NOTIFY_OPERATION(autoryzacjaUzytkownika, autoryzacjaUzytkownika(), 0, Default_TsystemCentralny_autoryzacjaUzytkownika_SERIALIZE);
    //#[ operation autoryzacjaUzytkownika()
    //#]
}

void TsystemCentralny::message_0() {
    NOTIFY_OPERATION(message_0, message_0(), 0, Default_TsystemCentralny_message_0_SERIALIZE);
    //#[ operation message_0()
    //#]
}

void TsystemCentralny::przetwarzajZapytanie() {
    NOTIFY_OPERATION(przetwarzajZapytanie, przetwarzajZapytanie(), 0, Default_TsystemCentralny_przetwarzajZapytanie_SERIALIZE);
    //#[ operation przetwarzajZapytanie()
    //#]
}

void TsystemCentralny::przkazDane() {
    NOTIFY_OPERATION(przkazDane, przkazDane(), 0, Default_TsystemCentralny_przkazDane_SERIALIZE);
    //#[ operation przkazDane()
    //#]
}

void TsystemCentralny::wprowadzenieDanych() {
    NOTIFY_OPERATION(wprowadzenieDanych, wprowadzenieDanych(), 0, Default_TsystemCentralny_wprowadzenieDanych_SERIALIZE);
    //#[ operation wprowadzenieDanych()
    //#]
}

void TsystemCentralny::wyslijOdpowiedz() {
    NOTIFY_OPERATION(wyslijOdpowiedz, wyslijOdpowiedz(), 0, Default_TsystemCentralny_wyslijOdpowiedz_SERIALIZE);
    //#[ operation wyslijOdpowiedz()
    //#]
}

void TsystemCentralny::wyslijZapytanie() {
    NOTIFY_OPERATION(wyslijZapytanie, wyslijZapytanie(), 0, Default_TsystemCentralny_wyslijZapytanie_SERIALIZE);
    //#[ operation wyslijZapytanie()
    //#]
}

TbazaDanych* TsystemCentralny::getItsTbazaDanych() const {
    return itsTbazaDanych;
}

void TsystemCentralny::setItsTbazaDanych(TbazaDanych* p_TbazaDanych) {
    itsTbazaDanych = p_TbazaDanych;
    if(p_TbazaDanych != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTbazaDanych", p_TbazaDanych, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTbazaDanych");
        }
}

TbazaDanych* TsystemCentralny::getItsTbazaDanych_1() const {
    return itsTbazaDanych_1;
}

void TsystemCentralny::setItsTbazaDanych_1(TbazaDanych* p_TbazaDanych) {
    itsTbazaDanych_1 = p_TbazaDanych;
    if(p_TbazaDanych != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTbazaDanych_1", p_TbazaDanych, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTbazaDanych_1");
        }
}

Tfotoradar* TsystemCentralny::getItsTfotoradar() const {
    return itsTfotoradar;
}

void TsystemCentralny::setItsTfotoradar(Tfotoradar* p_Tfotoradar) {
    if(p_Tfotoradar != NULL)
        {
            p_Tfotoradar->_setItsTsystemCentralny(this);
        }
    _setItsTfotoradar(p_Tfotoradar);
}

void TsystemCentralny::cleanUpRelations() {
    if(itsTbazaDanych != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTbazaDanych");
            itsTbazaDanych = NULL;
        }
    if(itsTbazaDanych_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTbazaDanych_1");
            itsTbazaDanych_1 = NULL;
        }
    if(itsTfotoradar != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTfotoradar");
            TsystemCentralny* p_TsystemCentralny = itsTfotoradar->getItsTsystemCentralny();
            if(p_TsystemCentralny != NULL)
                {
                    itsTfotoradar->__setItsTsystemCentralny(NULL);
                }
            itsTfotoradar = NULL;
        }
}

void TsystemCentralny::__setItsTfotoradar(Tfotoradar* p_Tfotoradar) {
    itsTfotoradar = p_Tfotoradar;
    if(p_Tfotoradar != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTfotoradar", p_Tfotoradar, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTfotoradar");
        }
}

void TsystemCentralny::_setItsTfotoradar(Tfotoradar* p_Tfotoradar) {
    if(itsTfotoradar != NULL)
        {
            itsTfotoradar->__setItsTsystemCentralny(NULL);
        }
    __setItsTfotoradar(p_Tfotoradar);
}

void TsystemCentralny::_clearItsTfotoradar() {
    NOTIFY_RELATION_CLEARED("itsTfotoradar");
    itsTfotoradar = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedTsystemCentralny::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsTbazaDanych", false, true);
    if(myReal->itsTbazaDanych)
        {
            aomsRelations->ADD_ITEM(myReal->itsTbazaDanych);
        }
    aomsRelations->addRelation("itsTbazaDanych_1", false, true);
    if(myReal->itsTbazaDanych_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsTbazaDanych_1);
        }
    aomsRelations->addRelation("itsTfotoradar", false, true);
    if(myReal->itsTfotoradar)
        {
            aomsRelations->ADD_ITEM(myReal->itsTfotoradar);
        }
}
//#]

IMPLEMENT_META_P(TsystemCentralny, Default, Default, false, OMAnimatedTsystemCentralny)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TsystemCentralny.cpp
*********************************************************************/
