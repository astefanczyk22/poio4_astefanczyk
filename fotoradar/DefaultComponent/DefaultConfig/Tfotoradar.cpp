/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Tfotoradar
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/Tfotoradar.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Tfotoradar.h"
//## link itsTczujnikPredkosci
#include "TczujnikPredkosci.h"
//## link itsTlokalnaBazaDanych
#include "TlokalnaBazaDanych.h"
//## link itsTsystemCentralny
#include "TsystemCentralny.h"
//#[ ignore
#define Default_Tfotoradar_Tfotoradar_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Tfotoradar
Tfotoradar::Tfotoradar() {
    NOTIFY_CONSTRUCTOR(Tfotoradar, Tfotoradar(), 0, Default_Tfotoradar_Tfotoradar_SERIALIZE);
    itsTczujnikPredkosci = NULL;
    itsTlokalnaBazaDanych = NULL;
    itsTsystemCentralny = NULL;
}

Tfotoradar::~Tfotoradar() {
    NOTIFY_DESTRUCTOR(~Tfotoradar, true);
    cleanUpRelations();
}

TczujnikPredkosci* Tfotoradar::getItsTczujnikPredkosci() const {
    return itsTczujnikPredkosci;
}

void Tfotoradar::setItsTczujnikPredkosci(TczujnikPredkosci* p_TczujnikPredkosci) {
    if(p_TczujnikPredkosci != NULL)
        {
            p_TczujnikPredkosci->_setItsTfotoradar(this);
        }
    _setItsTczujnikPredkosci(p_TczujnikPredkosci);
}

TlokalnaBazaDanych* Tfotoradar::getItsTlokalnaBazaDanych() const {
    return itsTlokalnaBazaDanych;
}

void Tfotoradar::setItsTlokalnaBazaDanych(TlokalnaBazaDanych* p_TlokalnaBazaDanych) {
    itsTlokalnaBazaDanych = p_TlokalnaBazaDanych;
    if(p_TlokalnaBazaDanych != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTlokalnaBazaDanych", p_TlokalnaBazaDanych, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTlokalnaBazaDanych");
        }
}

TsystemCentralny* Tfotoradar::getItsTsystemCentralny() const {
    return itsTsystemCentralny;
}

void Tfotoradar::setItsTsystemCentralny(TsystemCentralny* p_TsystemCentralny) {
    if(p_TsystemCentralny != NULL)
        {
            p_TsystemCentralny->_setItsTfotoradar(this);
        }
    _setItsTsystemCentralny(p_TsystemCentralny);
}

void Tfotoradar::cleanUpRelations() {
    if(itsTczujnikPredkosci != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTczujnikPredkosci");
            Tfotoradar* p_Tfotoradar = itsTczujnikPredkosci->getItsTfotoradar();
            if(p_Tfotoradar != NULL)
                {
                    itsTczujnikPredkosci->__setItsTfotoradar(NULL);
                }
            itsTczujnikPredkosci = NULL;
        }
    if(itsTlokalnaBazaDanych != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTlokalnaBazaDanych");
            itsTlokalnaBazaDanych = NULL;
        }
    if(itsTsystemCentralny != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsTsystemCentralny");
            Tfotoradar* p_Tfotoradar = itsTsystemCentralny->getItsTfotoradar();
            if(p_Tfotoradar != NULL)
                {
                    itsTsystemCentralny->__setItsTfotoradar(NULL);
                }
            itsTsystemCentralny = NULL;
        }
}

void Tfotoradar::__setItsTczujnikPredkosci(TczujnikPredkosci* p_TczujnikPredkosci) {
    itsTczujnikPredkosci = p_TczujnikPredkosci;
    if(p_TczujnikPredkosci != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTczujnikPredkosci", p_TczujnikPredkosci, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTczujnikPredkosci");
        }
}

void Tfotoradar::_setItsTczujnikPredkosci(TczujnikPredkosci* p_TczujnikPredkosci) {
    if(itsTczujnikPredkosci != NULL)
        {
            itsTczujnikPredkosci->__setItsTfotoradar(NULL);
        }
    __setItsTczujnikPredkosci(p_TczujnikPredkosci);
}

void Tfotoradar::_clearItsTczujnikPredkosci() {
    NOTIFY_RELATION_CLEARED("itsTczujnikPredkosci");
    itsTczujnikPredkosci = NULL;
}

void Tfotoradar::__setItsTsystemCentralny(TsystemCentralny* p_TsystemCentralny) {
    itsTsystemCentralny = p_TsystemCentralny;
    if(p_TsystemCentralny != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsTsystemCentralny", p_TsystemCentralny, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsTsystemCentralny");
        }
}

void Tfotoradar::_setItsTsystemCentralny(TsystemCentralny* p_TsystemCentralny) {
    if(itsTsystemCentralny != NULL)
        {
            itsTsystemCentralny->__setItsTfotoradar(NULL);
        }
    __setItsTsystemCentralny(p_TsystemCentralny);
}

void Tfotoradar::_clearItsTsystemCentralny() {
    NOTIFY_RELATION_CLEARED("itsTsystemCentralny");
    itsTsystemCentralny = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedTfotoradar::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsTlokalnaBazaDanych", false, true);
    if(myReal->itsTlokalnaBazaDanych)
        {
            aomsRelations->ADD_ITEM(myReal->itsTlokalnaBazaDanych);
        }
    aomsRelations->addRelation("itsTczujnikPredkosci", false, true);
    if(myReal->itsTczujnikPredkosci)
        {
            aomsRelations->ADD_ITEM(myReal->itsTczujnikPredkosci);
        }
    aomsRelations->addRelation("itsTsystemCentralny", false, true);
    if(myReal->itsTsystemCentralny)
        {
            aomsRelations->ADD_ITEM(myReal->itsTsystemCentralny);
        }
}
//#]

IMPLEMENT_META_P(Tfotoradar, Default, Default, false, OMAnimatedTfotoradar)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Tfotoradar.cpp
*********************************************************************/
