/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Traport
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/Traport.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Traport.h"
//## link itsToperator
#include "Toperator.h"
//#[ ignore
#define Default_Traport_Traport_SERIALIZE OM_NO_OP

#define OM_Default_Traport_Traport_1_SERIALIZE OM_NO_OP

#define Default_Traport_createRaport_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Traport
Traport::Traport() {
    NOTIFY_CONSTRUCTOR(Traport, Traport(), 0, Default_Traport_Traport_SERIALIZE);
    itsToperator = NULL;
}

Traport::~Traport() {
    NOTIFY_DESTRUCTOR(~Traport, true);
    cleanUpRelations();
}

void Traport::createRaport() {
    NOTIFY_OPERATION(createRaport, createRaport(), 0, Default_Traport_createRaport_SERIALIZE);
    //#[ operation createRaport()
    //#]
}

Toperator* Traport::getItsToperator() const {
    return itsToperator;
}

void Traport::setItsToperator(Toperator* p_Toperator) {
    if(p_Toperator != NULL)
        {
            p_Toperator->_setItsTraport(this);
        }
    _setItsToperator(p_Toperator);
}

void Traport::cleanUpRelations() {
    if(itsToperator != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsToperator");
            Traport* p_Traport = itsToperator->getItsTraport();
            if(p_Traport != NULL)
                {
                    itsToperator->__setItsTraport(NULL);
                }
            itsToperator = NULL;
        }
}

void Traport::__setItsToperator(Toperator* p_Toperator) {
    itsToperator = p_Toperator;
    if(p_Toperator != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsToperator", p_Toperator, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsToperator");
        }
}

void Traport::_setItsToperator(Toperator* p_Toperator) {
    if(itsToperator != NULL)
        {
            itsToperator->__setItsTraport(NULL);
        }
    __setItsToperator(p_Toperator);
}

void Traport::_clearItsToperator() {
    NOTIFY_RELATION_CLEARED("itsToperator");
    itsToperator = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedTraport::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsToperator", false, true);
    if(myReal->itsToperator)
        {
            aomsRelations->ADD_ITEM(myReal->itsToperator);
        }
}
//#]

IMPLEMENT_META_P(Traport, Default, Default, false, OMAnimatedTraport)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Traport.cpp
*********************************************************************/
