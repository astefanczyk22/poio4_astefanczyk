
############# Target type (Debug/Release) ##################
############################################################
CPPCompileDebug=-g
CPPCompileRelease=-O
LinkDebug=-g
LinkRelease=-O

CleanupFlagForSimulink=
SIMULINK_CONFIG=False
ifeq ($(SIMULINK_CONFIG),True)
CleanupFlagForSimulink=-DOM_WITH_CLEANUP
endif

ConfigurationCPPCompileSwitches=   $(INCLUDE_QUALIFIER). $(INCLUDE_QUALIFIER)$(OMROOT) $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/oxf $(DEFINE_QUALIFIER)CYGWIN $(INST_FLAGS) $(INCLUDE_PATH) $(INST_INCLUDES) -Wno-write-strings $(CPPCompileDebug) -c  $(CleanupFlagForSimulink)
ConfigurationCCCompileSwitches=$(INCLUDE_PATH) -c 

#########################################
###### Predefined macros ################
RM=/bin/rm -rf
INCLUDE_QUALIFIER=-I
DEFINE_QUALIFIER=-D
CC=g++
LIB_CMD=ar
LINK_CMD=g++
LIB_FLAGS=rvu
LINK_FLAGS= $(LinkDebug)   

#########################################
####### Context macros ##################

FLAGSFILE=
RULESFILE=
OMROOT="C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share"
RHPROOT="C:/Program Files/IBM/Rational/Rhapsody/8.3.1"

CPP_EXT=.cpp
H_EXT=.h
OBJ_EXT=.o
EXE_EXT=.exe
LIB_EXT=.a

INSTRUMENTATION=Animation

TIME_MODEL=RealTime

TARGET_TYPE=Executable

TARGET_NAME=DefaultComponent

all : $(TARGET_NAME)$(EXE_EXT) DefaultComponent.mak

TARGET_MAIN=MainDefaultComponent

LIBS=

INCLUDE_PATH= \
  $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/osconfig/Cygwin

ADDITIONAL_OBJS=

OBJS= \
  Toperator.o \
  Traport.o \
  TsystemCentralny.o \
  TbazaDanych.o \
  Tfotoradar.o \
  TczujnikPredkosci.o \
  TlokalnaBazaDanych.o \
  TrejestracjaObrazu.o \
  Default.o




#########################################
####### Predefined macros ###############
$(OBJS) : $(INST_LIBS) $(OXF_LIBS)

ifeq ($(INSTRUMENTATION),Animation)

INST_FLAGS=$(DEFINE_QUALIFIER)OMANIMATOR $(DEFINE_QUALIFIER)__USE_W32_SOCKETS 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinaomanim$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),Tracing)

INST_FLAGS=$(DEFINE_QUALIFIER)OMTRACER 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwintomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinaomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),None)

INST_FLAGS= 
INST_INCLUDES=
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxsim$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxf$(LIB_EXT)
SOCK_LIB=-lws2_32

else
	@echo An invalid Instrumentation $(INSTRUMENTATION) is specified.
	exit
endif
endif
endif

.SUFFIXES: $(CPP_EXT)

#####################################################################
##################### Context dependencies and commands #############






Toperator.o : Toperator.cpp Toperator.h    Default.h Traport.h TsystemCentralny.h 
	@echo Compiling Toperator.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Toperator.o Toperator.cpp




Traport.o : Traport.cpp Traport.h    Default.h Toperator.h 
	@echo Compiling Traport.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Traport.o Traport.cpp




TsystemCentralny.o : TsystemCentralny.cpp TsystemCentralny.h    Default.h TbazaDanych.h Tfotoradar.h 
	@echo Compiling TsystemCentralny.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o TsystemCentralny.o TsystemCentralny.cpp




TbazaDanych.o : TbazaDanych.cpp TbazaDanych.h    Default.h 
	@echo Compiling TbazaDanych.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o TbazaDanych.o TbazaDanych.cpp




Tfotoradar.o : Tfotoradar.cpp Tfotoradar.h    Default.h TlokalnaBazaDanych.h TczujnikPredkosci.h TsystemCentralny.h 
	@echo Compiling Tfotoradar.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Tfotoradar.o Tfotoradar.cpp




TczujnikPredkosci.o : TczujnikPredkosci.cpp TczujnikPredkosci.h    Default.h Tfotoradar.h TrejestracjaObrazu.h 
	@echo Compiling TczujnikPredkosci.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o TczujnikPredkosci.o TczujnikPredkosci.cpp




TlokalnaBazaDanych.o : TlokalnaBazaDanych.cpp TlokalnaBazaDanych.h    Default.h 
	@echo Compiling TlokalnaBazaDanych.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o TlokalnaBazaDanych.o TlokalnaBazaDanych.cpp




TrejestracjaObrazu.o : TrejestracjaObrazu.cpp TrejestracjaObrazu.h    Default.h 
	@echo Compiling TrejestracjaObrazu.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o TrejestracjaObrazu.o TrejestracjaObrazu.cpp




Default.o : Default.cpp Default.h    Toperator.h Traport.h TsystemCentralny.h TbazaDanych.h Tfotoradar.h TczujnikPredkosci.h TlokalnaBazaDanych.h TrejestracjaObrazu.h 
	@echo Compiling Default.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Default.o Default.cpp







$(TARGET_MAIN)$(OBJ_EXT) : $(TARGET_MAIN)$(CPP_EXT) $(OBJS)
	@echo Compiling $(TARGET_MAIN)$(CPP_EXT)
	@$(CC) $(ConfigurationCPPCompileSwitches) -o  $(TARGET_MAIN)$(OBJ_EXT) $(TARGET_MAIN)$(CPP_EXT)

####################################################################
############## Predefined Instructions #############################
$(TARGET_NAME)$(EXE_EXT): $(OBJS) $(ADDITIONAL_OBJS) $(TARGET_MAIN)$(OBJ_EXT) DefaultComponent.mak
	@echo Linking $(TARGET_NAME)$(EXE_EXT)
	@$(LINK_CMD)  $(TARGET_MAIN)$(OBJ_EXT) $(OBJS) $(ADDITIONAL_OBJS) \
	$(LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(SOCK_LIB) \
	$(LINK_FLAGS) -o $(TARGET_NAME)$(EXE_EXT)

$(TARGET_NAME)$(LIB_EXT) : $(OBJS) $(ADDITIONAL_OBJS) DefaultComponent.mak
	@echo Building library $@
	@$(LIB_CMD) $(LIB_FLAGS) $(TARGET_NAME)$(LIB_EXT) $(OBJS) $(ADDITIONAL_OBJS)



clean:
	@echo Cleanup
	$(RM) Toperator.o
	$(RM) Traport.o
	$(RM) TsystemCentralny.o
	$(RM) TbazaDanych.o
	$(RM) Tfotoradar.o
	$(RM) TczujnikPredkosci.o
	$(RM) TlokalnaBazaDanych.o
	$(RM) TrejestracjaObrazu.o
	$(RM) Default.o
	$(RM) $(TARGET_MAIN)$(OBJ_EXT) $(ADDITIONAL_OBJS)
	$(RM) $(TARGET_NAME)$(LIB_EXT)
	$(RM) $(TARGET_NAME)$(EXE_EXT)

