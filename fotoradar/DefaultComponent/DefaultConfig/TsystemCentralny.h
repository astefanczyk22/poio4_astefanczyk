/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: TsystemCentralny
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/TsystemCentralny.h
*********************************************************************/

#ifndef TsystemCentralny_H
#define TsystemCentralny_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## link itsTbazaDanych
class TbazaDanych;

//## link itsTfotoradar
class Tfotoradar;

//## package Default

//## class TsystemCentralny
class TsystemCentralny {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedTsystemCentralny;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    TsystemCentralny();
    
    //## auto_generated
    ~TsystemCentralny();
    
    ////    Operations    ////
    
    //## operation autoryzacjaUytkownika()
    void autoryzacjaUytkownika();
    
    //## operation autoryzacjaUzytkownika()
    void autoryzacjaUzytkownika();
    
    //## operation message_0()
    void message_0();
    
    //## operation przetwarzajZapytanie()
    void przetwarzajZapytanie();
    
    //## operation przkazDane()
    void przkazDane();
    
    //## operation wprowadzenieDanych()
    void wprowadzenieDanych();
    
    //## operation wyslijOdpowiedz()
    void wyslijOdpowiedz();
    
    //## operation wyslijZapytanie()
    void wyslijZapytanie();
    
    ////    Additional operations    ////
    
    //## auto_generated
    TbazaDanych* getItsTbazaDanych() const;
    
    //## auto_generated
    void setItsTbazaDanych(TbazaDanych* p_TbazaDanych);
    
    //## auto_generated
    TbazaDanych* getItsTbazaDanych_1() const;
    
    //## auto_generated
    void setItsTbazaDanych_1(TbazaDanych* p_TbazaDanych);
    
    //## auto_generated
    Tfotoradar* getItsTfotoradar() const;
    
    //## auto_generated
    void setItsTfotoradar(Tfotoradar* p_Tfotoradar);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    TbazaDanych* itsTbazaDanych;		//## link itsTbazaDanych
    
    TbazaDanych* itsTbazaDanych_1;		//## link itsTbazaDanych_1
    
    Tfotoradar* itsTfotoradar;		//## link itsTfotoradar
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsTfotoradar(Tfotoradar* p_Tfotoradar);
    
    //## auto_generated
    void _setItsTfotoradar(Tfotoradar* p_Tfotoradar);
    
    //## auto_generated
    void _clearItsTfotoradar();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedTsystemCentralny : virtual public AOMInstance {
    DECLARE_META(TsystemCentralny, OMAnimatedTsystemCentralny)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/TsystemCentralny.h
*********************************************************************/
