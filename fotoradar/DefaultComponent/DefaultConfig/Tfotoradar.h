/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: admin
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Tfotoradar
//!	Generated Date	: Wed, 28, Jun 2023  
	File Path	: DefaultComponent/DefaultConfig/Tfotoradar.h
*********************************************************************/

#ifndef Tfotoradar_H
#define Tfotoradar_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## link itsTczujnikPredkosci
class TczujnikPredkosci;

//## link itsTlokalnaBazaDanych
class TlokalnaBazaDanych;

//## link itsTsystemCentralny
class TsystemCentralny;

//## package Default

//## class Tfotoradar
class Tfotoradar {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedTfotoradar;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Tfotoradar();
    
    //## auto_generated
    ~Tfotoradar();
    
    ////    Additional operations    ////
    
    //## auto_generated
    TczujnikPredkosci* getItsTczujnikPredkosci() const;
    
    //## auto_generated
    void setItsTczujnikPredkosci(TczujnikPredkosci* p_TczujnikPredkosci);
    
    //## auto_generated
    TlokalnaBazaDanych* getItsTlokalnaBazaDanych() const;
    
    //## auto_generated
    void setItsTlokalnaBazaDanych(TlokalnaBazaDanych* p_TlokalnaBazaDanych);
    
    //## auto_generated
    TsystemCentralny* getItsTsystemCentralny() const;
    
    //## auto_generated
    void setItsTsystemCentralny(TsystemCentralny* p_TsystemCentralny);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    TczujnikPredkosci* itsTczujnikPredkosci;		//## link itsTczujnikPredkosci
    
    TlokalnaBazaDanych* itsTlokalnaBazaDanych;		//## link itsTlokalnaBazaDanych
    
    TsystemCentralny* itsTsystemCentralny;		//## link itsTsystemCentralny
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsTczujnikPredkosci(TczujnikPredkosci* p_TczujnikPredkosci);
    
    //## auto_generated
    void _setItsTczujnikPredkosci(TczujnikPredkosci* p_TczujnikPredkosci);
    
    //## auto_generated
    void _clearItsTczujnikPredkosci();
    
    //## auto_generated
    void __setItsTsystemCentralny(TsystemCentralny* p_TsystemCentralny);
    
    //## auto_generated
    void _setItsTsystemCentralny(TsystemCentralny* p_TsystemCentralny);
    
    //## auto_generated
    void _clearItsTsystemCentralny();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedTfotoradar : virtual public AOMInstance {
    DECLARE_META(Tfotoradar, OMAnimatedTfotoradar)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Tfotoradar.h
*********************************************************************/
