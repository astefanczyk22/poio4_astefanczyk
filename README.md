# POIO4_AStefanczyk

# Fotoradar

## Wymagania:
1. Rejestracja prędkości pojazdu i wykrywanie naruszeń przepisów drogowych
2. Możliwość przechowywania danych dotyczących wykrytych naruszeń
3. Musi posiadać interfejs do pobierania danych z radaru
4. Musi posiadać interfejs do wysyłania danych do systemu centralnego w celu dalszej analizy i przetwarzania
5. Musi posiadać system diagnostyczny i alarmów, które informują operatorów o problemach z systemem
6. System zabezpieczeń przed nieautoryzowanym dostępem
7. Możliwość aktualizacji oprogramowania w celu poprawy wydajności i funkcjonalności

## Dostępne operacje:
### Operator radaru
1. Konfiguracja radaru, takiej jak zmiana kąta widzenia itp.
2. Usunięcie i przeglądanie historii pomiarów prędkości pojazdów
3. Generowanie raportów na temat pomiarów prędkości pojazdów

### Serwisant
1. Przeprowadzanie diagnozy i napraw radaru
2. Aktualizacja oprogramowania radaru

### Administrator systemu
1. Konfiguracja połączeń między radarami a systemem centralnym
2. Zapewnienie bezpieczeństwa systemu

## Authors and acknowledgment
Artur Stefańczyk

## Project status
In progress
